# README #

## Description ##

This is the dataset used in the paper "Constrained Co-embedding Model for User Profiling in Question Answering Communities" and "Flow-based Constrained Co-embedding Model for User Profiling in Question Answering Communities," which is crawled from the Chinese Q&A community, e.g., ZHIHU. It contains all the answers from 2692 users and the corresponding voting information.